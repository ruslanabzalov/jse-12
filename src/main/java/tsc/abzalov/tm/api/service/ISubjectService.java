package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Subject;

import java.util.List;

public interface ISubjectService<T extends Subject> {

    int size();

    boolean create(T t);

    List<T> getAll();

    T getById(String id);

    T getByIndex(int index);

    List<T> getByName(String name);

    boolean updateById(String id, String name, String description);

    boolean updateByIndex(int index, String name, String description);

    void removeAll();

    boolean removeById(String id);

    boolean removeByIndex(int index);

    boolean removeByName(String name);

    boolean startById(String id);

    boolean endById(String id);

}
