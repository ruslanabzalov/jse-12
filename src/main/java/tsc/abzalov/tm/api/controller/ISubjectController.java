package tsc.abzalov.tm.api.controller;

public interface ISubjectController {

    void create();

    void showAll();

    void showById();

    void showByIndex();

    void showByName();

    void editById();

    void editByIndex();

    void removeAll();

    void removeById();

    void removeByIndex();

    void removeByName();

    void startById();

    void endById();

}
