package tsc.abzalov.tm.constant;

public interface ApplicationCommandConst {

    String CMD_HELP = "help";

    String CMD_INFO = "info";

    String CMD_ABOUT = "about";

    String CMD_VERSION = "version";

    String CMD_COMMANDS = "commands";

    String CMD_ARGUMENTS = "arguments";

    String CMD_EXIT = "exit";

    String CMD_CREATE_PROJECT = "create-project";

    String CMD_SHOW_ALL_PROJECTS = "show-all-projects";

    String CMD_SHOW_PROJECT_BY_ID = "show-project-by-id";

    String CMD_SHOW_PROJECT_BY_INDEX = "show-project-by-index";

    String CMD_SHOW_PROJECTS_BY_NAME = "show-projects-by-name";

    String CMD_UPDATE_PROJECT_BY_ID = "update-project-by-id";

    String CMD_UPDATE_PROJECT_BY_INDEX = "update-project-by-index";

    String CMD_DELETE_ALL_PROJECTS = "delete-all-projects";

    String CMD_DELETE_PROJECT_BY_ID = "delete-project-by-id";

    String CMD_DELETE_PROJECT_BY_INDEX = "delete-project-by-index";

    String CMD_DELETE_PROJECTS_BY_NAME = "delete-projects-by-name";

    String CMD_START_PROJECT = "start-project";

    String CMD_END_PROJECT = "end-project";

    String CMD_CREATE_TASK = "create-task";

    String CMD_SHOW_ALL_TASKS = "show-all-tasks";

    String CMD_SHOW_TASK_BY_ID = "show-task-by-id";

    String CMD_SHOW_TASK_BY_INDEX = "show-task-by-index";

    String CMD_SHOW_TASKS_BY_NAME = "show-tasks-by-name";

    String CMD_UPDATE_TASK_BY_ID = "update-task-by-id";

    String CMD_UPDATE_TASK_BY_INDEX = "update-task-by-index";

    String CMD_DELETE_ALL_TASKS = "delete-all-tasks";

    String CMD_DELETE_TASK_BY_ID = "delete-task-by-id";

    String CMD_DELETE_TASK_BY_INDEX = "delete-task-by-index";

    String CMD_DELETE_TASKS_BY_NAME = "delete-tasks-by-name";

    String CMD_START_TASK = "start-task";

    String CMD_END_TASK = "end-task";

}