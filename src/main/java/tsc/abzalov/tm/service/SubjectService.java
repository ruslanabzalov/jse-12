package tsc.abzalov.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ISubjectRepository;
import tsc.abzalov.tm.api.service.ISubjectService;
import tsc.abzalov.tm.model.Subject;

import java.util.List;

import static tsc.abzalov.tm.util.InputUtil.isCorrectIndex;

public class SubjectService<T extends Subject> implements ISubjectService<T> {

    @NotNull
    private final ISubjectRepository<T> repository;

    public SubjectService(@NotNull ISubjectRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public int size() {
        return repository.size();
    }

    @Override
    public boolean create(@NotNull T subject) {
        return repository.add(subject);
    }

    @Override
    public List<T> getAll() {
        return repository.findAll();
    }

    @Override
    @Nullable
    public T getById(@NotNull String id) {
        if (StringUtils.isAnyBlank(id)) return null;
        return repository.findById(id);
    }

    @Override
    @Nullable
    public T getByIndex(int index) {
        final boolean isIndexCorrect = isCorrectIndex(index, repository.size());
        if (!isIndexCorrect) return null;
        return repository.findByIndex(index);
    }

    @Override
    @Nullable
    public List<T> getByName(@NotNull String name) {
        if (StringUtils.isAnyBlank(name)) return null;
        return repository.findByName(name);
    }

    @Override
    public boolean updateById(@NotNull String id, @NotNull String name, @NotNull String description) {
        if (StringUtils.isAnyBlank(id, name)) return false;
        if (StringUtils.isBlank(description)) description = "Description is empty";
        return repository.updateById(id, name, description);
    }

    @Override
    public boolean updateByIndex(int index, @NotNull String name, @NotNull String description) {
        final boolean isIndexCorrect = isCorrectIndex(index, repository.size());
        if (!isIndexCorrect || StringUtils.isBlank(name)) return false;
        if (StringUtils.isBlank(description)) description = "Description is empty";
        return repository.updateByIndex(index, name, description);
    }

    @Override
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    public boolean removeById(@NotNull String id) {
        if (StringUtils.isAnyBlank(id)) return false;
        return repository.deleteById(id);
    }

    @Override
    public boolean removeByIndex(int index) {
        final boolean isIndexCorrect = isCorrectIndex(index, repository.size());
        if (!isIndexCorrect) return false;
        return repository.deleteByIndex(index);
    }

    @Override
    public boolean removeByName(@NotNull String name) {
        if (StringUtils.isBlank(name)) return false;
        return repository.deleteByName(name);
    }

    @Override
    public boolean startById(@NotNull String id) {
        if (StringUtils.isBlank(id)) return false;
        return repository.startById(id);
    }

    @Override
    public boolean endById(@NotNull String id) {
        if (StringUtils.isBlank(id)) return false;
        return repository.endById(id);
    }

}
