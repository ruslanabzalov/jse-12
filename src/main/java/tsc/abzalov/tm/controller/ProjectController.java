package tsc.abzalov.tm.controller;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.controller.ISubjectController;
import tsc.abzalov.tm.api.service.ISubjectService;
import tsc.abzalov.tm.model.Project;

import java.util.List;

import static tsc.abzalov.tm.util.InputUtil.*;

public class ProjectController implements ISubjectController {

    @NotNull
    private final ISubjectService<Project> projectService;

    public ProjectController(@NotNull ISubjectService<Project> projectService) {
        this.projectService = projectService;
    }

    @Override
    public void create() {
        System.out.println("PROJECT CREATION\n");
        final String projectName = inputName();

        System.out.print("Please, enter project description: ");
        final String projectDescription = INPUT.nextLine();

        final boolean isProjectCreated = (StringUtils.isBlank(projectDescription))
                ? projectService.create(new Project(projectName))
                : projectService.create(new Project(projectName, projectDescription));

        if (isProjectCreated) {
            System.out.println("Project was successfully created.\n");
            return;
        }

        System.out.println("Project was not created! Please, try create new project.\n");
    }

    @Override
    public void showAll() {
        System.out.println("ALL PROJECTS LIST\n");
        final List<Project> foundedProjects = projectService.getAll();
        if (foundedProjects.size() == 0) {
            System.out.println("Projects list is empty.\n");
            return;
        }

        foundedProjects.forEach(project -> System.out.println((foundedProjects.indexOf(project) + 1) + ". " + project));
        System.out.println();
    }

    @Override
    public void showById() {
        System.out.println("FIND PROJECT BY ID\n");
        final String projectId = inputId();
        System.out.println();

        final Project foundedProject = projectService.getById(projectId);
        if (foundedProject == null) {
            System.out.println("Searched project was not found.\n");
            return;
        }

        System.out.println((projectService.getAll().indexOf(foundedProject) + 1) + ". " + foundedProject + "\n");
    }

    @Override
    public void showByIndex() {
        System.out.println("FIND PROJECT BY INDEX\n");
        final int projectIndex = inputIndex();
        System.out.println();

        final Project foundedProject = projectService.getByIndex(projectIndex);
        if (foundedProject == null) {
            System.out.println("Searched project was not found.\n");
            return;
        }

        System.out.println((projectService.getAll().indexOf(foundedProject) + 1) + ". " + foundedProject + "\n");
    }

    @Override
    public void showByName() {
        System.out.println("FIND PROJECTS BY NAME\n");
        final String projectName = inputName();
        System.out.println();

        final List<Project> foundedProjects = projectService.getByName(projectName);
        if (foundedProjects.size() == 0) {
            System.out.println("Searched projects were not found.");
            return;
        }

        foundedProjects.forEach(project ->
                    System.out.println((projectService.getAll().indexOf(project) + 1) + ". " + project.toString()));
        System.out.println();
    }

    @Override
    public void editById() {
        System.out.println("EDIT PROJECT BY ID\n");
        final String projectId = inputId();
        final String projectName = inputName();
        final String projectDescription = inputDescription();
        System.out.println();

        final boolean wasProjectUpdated = projectService.updateById(projectId, projectName, projectDescription);
        if (wasProjectUpdated) {
            System.out.println("Project was successfully updated.");
            return;
        }

        System.out.println("Project was not updated! Please, try again.");
    }

    @Override
    public void editByIndex() {
        System.out.println("EDIT PROJECT BY INDEX\n");
        final int projectIndex = inputIndex();
        final String projectName = inputName();
        final String projectDescription = inputDescription();
        System.out.println();

        final boolean wasProjectUpdated = projectService.updateByIndex(projectIndex, projectName, projectDescription);
        if (wasProjectUpdated) {
            System.out.println("Project was successfully updated.");
            return;
        }

        System.out.println("Project was not updated! Please, try again.");
    }

    @Override
    public void removeAll() {
        System.out.println("PROJECTS DELETION\n");
        if (projectService.size() == 0) {
            System.out.println("Projects list is already empty!");
            return;
        }

        projectService.removeAll();
        System.out.println("All projects were successfully deleted.\n");
    }

    @Override
    public void removeById() {
        System.out.println("DELETE PROJECT BY ID\n");
        final boolean isProjectDeleted = projectService.removeById(inputId());
        if (isProjectDeleted) {
            System.out.println("Project was successfully deleted.\n");
            return;
        }

        System.out.println("Project was not deleted! Please, check that project exists and try again.\n");
    }

    @Override
    public void removeByIndex() {
        System.out.println("DELETE PROJECT BY INDEX\n");
        final boolean isProjectDeleted = projectService.removeByIndex(inputIndex());
        if (isProjectDeleted) {
            System.out.println("Project was successfully deleted.\n");
            return;
        }

        System.out.println("Project was not deleted! Please, check that project exists and try again.\n");
    }

    @Override
    public void removeByName() {
        System.out.println("DELETE PROJECTS BY NAME\n");
        final boolean areProjectsDeleted = projectService.removeByName(inputName());
        if (areProjectsDeleted) {
            System.out.println("Projects were successfully deleted.\n");
            return;
        }

        System.out.println("Projects were not deleted! Please, check that projects exist and try again.\n");
    }

    @Override
    public void startById() {
        System.out.println("START PROJECT BY ID\n");
        final boolean wasProjectStarted = projectService.startById(inputId());
        if (wasProjectStarted) {
            System.out.println("Project was successfully started.\n");
            return;
        }

        System.out.println("Project was not started! Please, check that project exists or it has correct status.\n");
    }

    @Override
    public void endById() {
        System.out.println("END PROJECT BY ID\n");
        final boolean wasProjectEnded = projectService.endById(inputId());
        if (wasProjectEnded) {
            System.out.println("Project was successfully ended.\n");
            return;
        }

        System.out.println("Project was not ended! Please, check that project exists or it has correct status.\n");
    }

}
