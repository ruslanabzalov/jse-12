package tsc.abzalov.tm.controller;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.controller.ISubjectController;
import tsc.abzalov.tm.api.service.ISubjectService;
import tsc.abzalov.tm.model.Task;

import java.util.List;

import static tsc.abzalov.tm.util.InputUtil.*;

public class TaskController implements ISubjectController {

    @NotNull
    private final ISubjectService<Task> taskService;

    public TaskController(@NotNull ISubjectService<Task> taskService) {
        this.taskService = taskService;
    }

    @Override
    public void create() {
        System.out.println("TASK CREATION\n");
        final String taskName = inputName();

        System.out.print("Please, enter task description: ");
        final String taskDescription = INPUT.nextLine();

        final boolean isTaskCreated = (StringUtils.isBlank(taskDescription))
                ? taskService.create(new Task(taskName))
                : taskService.create(new Task(taskName, taskDescription));

        if (isTaskCreated) {
            System.out.println("Task was successfully created.\n");
            return;
        }

        System.out.println("Task was not created! Please, try create new task.\n");
    }

    @Override
    public void showAll() {
        System.out.println("ALL TASKS LIST\n");
        final List<Task> foundedTasks = taskService.getAll();
        if (foundedTasks.size() == 0) {
            System.out.println("Tasks list is empty.\n");
            return;
        }

        foundedTasks.forEach(task -> System.out.println((foundedTasks.indexOf(task) + 1) + ". " + task));
        System.out.println();
    }

    @Override
    public void showById() {
        System.out.println("FIND TASK BY ID\n");
        final String taskId = inputId();
        System.out.println();

        final Task foundedTask = taskService.getById(taskId);
        if (foundedTask == null) {
            System.out.println("Searched task was not found.\n");
            return;
        }

        System.out.println((taskService.getAll().indexOf(foundedTask) + 1) + ". " + foundedTask + "\n");
    }

    @Override
    public void showByIndex() {
        System.out.println("FIND TASK BY INDEX\n");
        final int taskIndex = inputIndex();
        System.out.println();

        final Task foundedTask = taskService.getByIndex(taskIndex);
        if (foundedTask == null) {
            System.out.println("Searched task was not found.\n");
            return;
        }

        System.out.println((taskService.getAll().indexOf(foundedTask) + 1) + ". " + foundedTask + "\n");
    }

    @Override
    public void showByName() {
        System.out.println("FIND TASKS BY NAME\n");
        final String taskName = inputName();
        System.out.println();

        final List<Task> foundedTasks = taskService.getByName(taskName);
        if (foundedTasks.size() == 0) {
            System.out.println("Searched tasks were not found.");
            return;
        }

        foundedTasks.forEach(task ->
                System.out.println((taskService.getAll().indexOf(task) + 1) + ". " + task.toString()));
        System.out.println();
    }

    @Override
    public void editById() {
        System.out.println("EDIT TASK BY ID\n");
        final String taskId = inputId();
        final String taskName = inputName();
        final String taskDescription = inputDescription();
        System.out.println();

        final boolean wasTaskUpdated = taskService.updateById(taskId, taskName, taskDescription);
        if (wasTaskUpdated) {
            System.out.println("Task was successfully updated.");
            return;
        }

        System.out.println("Task was not updated! Please, try again.");
    }

    @Override
    public void editByIndex() {
        System.out.println("EDIT TASK BY INDEX\n");
        final int taskIndex = inputIndex();
        final String taskName = inputName();
        final String taskDescription = inputDescription();
        System.out.println();

        final boolean wasTaskUpdated = taskService.updateByIndex(taskIndex, taskName, taskDescription);
        if (wasTaskUpdated) {
            System.out.println("Task was successfully updated.");
            return;
        }

        System.out.println("Task was not updated! Please, try again.");
    }

    @Override
    public void removeAll() {
        System.out.println("TASKS DELETION\n");
        if (taskService.size() == 0) {
            System.out.println("Tasks list is already empty!");
            return;
        }

        taskService.removeAll();
        System.out.println("All tasks were successfully deleted.\n");
    }

    @Override
    public void removeById() {
        System.out.println("DELETE TASK BY ID\n");
        final boolean wasTaskDeleted = taskService.removeById(inputId());
        if (wasTaskDeleted) {
            System.out.println("Task was successfully deleted.\n");
            return;
        }

        System.out.println("Task was not deleted! Please, check that task exists and try again.\n");
    }

    @Override
    public void removeByIndex() {
        System.out.println("DELETE TASK BY INDEX\n");
        final boolean wasTaskDeleted = taskService.removeByIndex(inputIndex());
        if (wasTaskDeleted) {
            System.out.println("Task was successfully deleted.\n");
            return;
        }

        System.out.println("Task was not deleted! Please, check that task exists and try again.\n");
    }

    @Override
    public void removeByName() {
        System.out.println("DELETE TASKS BY NAME\n");
        final boolean wereTasksDeleted = taskService.removeByName(inputName());
        if (wereTasksDeleted) {
            System.out.println("Tasks was successfully deleted.\n");
            return;
        }

        System.out.println("Tasks were not deleted! Please, check that tasks exist and try again.\n");
    }

    @Override
    public void startById() {
        System.out.println("START TASK BY ID\n");
        final boolean wasTaskStarted = taskService.startById(inputId());
        if (wasTaskStarted) {
            System.out.println("Task was successfully started.\n");
            return;
        }

        System.out.println("Task was not started! Please, check that task exists or it has correct status.\n");
    }

    @Override
    public void endById() {
        System.out.println("END TASK BY ID\n");
        final boolean wasTaskEnded = taskService.endById(inputId());
        if (wasTaskEnded) {
            System.out.println("Task was successfully ended.\n");
            return;
        }

        System.out.println("Task was not ended! Please, check that task exists or it has correct status.\n");
    }

}
