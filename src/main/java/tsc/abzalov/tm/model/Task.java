package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;

public class Task extends Subject {

    private Task() {
    }

    public Task(@NotNull String name) {
        this.name = name;
    }

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

}
