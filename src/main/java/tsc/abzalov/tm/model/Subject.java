package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.model.enumeration.Status;

import java.util.UUID;

import static tsc.abzalov.tm.model.enumeration.Status.TODO;

public abstract class Subject {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "Description is empty";

    @NotNull
    protected Status status = TODO;

    @NotNull
    public String getId() {
        return this.id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return this.name +
                ": [ID: " + this.id +
                "; Description: " + this.description +
                "; Status: " + this.status.getDisplayName() + "]";
    }

}
