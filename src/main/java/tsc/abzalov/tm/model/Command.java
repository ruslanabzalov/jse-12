package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.enumeration.CommandType;

public class Command {

    @NotNull
    private String name = "";

    @Nullable
    private String arg = "";

    @NotNull
    private String description = "";

    @NotNull
    private CommandType commandType;

    private Command() {
    }

    public Command(@NotNull String name, @Nullable String arg, @NotNull String description,
                   @NotNull CommandType commandType) {
        this.name = name;
        this.arg = arg;
        this.description = description;
        this.commandType = commandType;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @Nullable
    public String getArg() {
        return this.arg;
    }

    public void setArg(@Nullable String arg) {
        this.arg = arg;
    }

    @NotNull
    public String getDescription() {
        return this.description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @NotNull
    public CommandType getCommandType() {
        return commandType;
    }

    @Override
    public String toString() {
        final String correctArg = (this.arg == null || this.arg.isEmpty()) ? ": " : " [" + this.arg + "]: ";
        return this.name + correctArg + this.description;
    }

}
