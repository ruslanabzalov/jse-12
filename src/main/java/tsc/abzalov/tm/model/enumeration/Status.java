package tsc.abzalov.tm.model.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Status {

    TODO("TODO"),
    IN_PROGRESS("In Progress"),
    DONE("Done");

    @NotNull
    private final String displayName;

    Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
