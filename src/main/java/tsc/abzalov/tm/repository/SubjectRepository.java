package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ISubjectRepository;
import tsc.abzalov.tm.model.Subject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static tsc.abzalov.tm.model.enumeration.Status.*;

public class SubjectRepository<T extends Subject> implements ISubjectRepository<T> {

    private final List<T> subjects = new ArrayList<>();

    @Override
    public int size() {
        return subjects.size();
    }

    @Override
    public boolean add(@NotNull T t) {
        return subjects.add(t);
    }

    @Override
    @NotNull
    public List<T> findAll() {
        return subjects;
    }

    @Override
    @Nullable
    public T findById(@NotNull String id) {
        for (final T subject : subjects) {
            if (subject.getId().equals(id)) return subject;
        }
        return null;
    }

    @Override
    @Nullable
    public T findByIndex(int index) {
        return subjects.get(index);
    }

    @Override
    @NotNull
    public List<T> findByName(@NotNull String name) {
        return subjects.stream()
                .filter(subject -> subject.getName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public boolean updateById(@NotNull String id, @NotNull String name, @NotNull String description) {
        return wasSubjectUpdated(findById(id), name, description);
    }

    @Override
    public boolean updateByIndex(int index, @NotNull String name, @NotNull String description) {
        return wasSubjectUpdated(findByIndex(index), name, description);
    }

    @Override
    public void deleteAll() {
        subjects.clear();
    }

    @Override
    public boolean deleteById(String id) {
        return subjects.removeIf(subject -> subject.getId().equals(id));
    }

    @Override
    public boolean deleteByIndex(int index) {
        if (subjects.get(index) == null) return false;

        subjects.remove(index);
        return true;
    }

    @Override
    public boolean deleteByName(String name) {
        return subjects.removeIf(subject -> subject.getName().equals(name));
    }

    @Override
    public boolean startById(@NotNull String id) {
        for (final Subject subject : subjects) {
            if (subject.getId().equals(id)) {
                if (subject.getStatus() != TODO) return false;

                subject.setStatus(IN_PROGRESS);
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean endById(@NotNull String id) {
        for (final Subject subject : subjects) {
            if (subject.getId().equals(id)) {
                if (subject.getStatus() != IN_PROGRESS) return false;

                subject.setStatus(DONE);
                return true;
            }
        }

        return false;
    }

    private boolean wasSubjectUpdated(@Nullable T subject, @NotNull String name, @NotNull String description) {
        if (subject == null) return false;

        subject.setName(name);
        subject.setDescription(description);
        return true;
    }

}
